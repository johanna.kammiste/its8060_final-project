import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.log4j._
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.apache.spark.streaming.twitter._
import org.apache.spark.streaming.{Seconds, StreamingContext}

import java.util.Properties

object ProducerLayer {

  def main(args: Array[String]): Unit = {
    System.setProperty("hadoop.home.dir", "file:///C:/winutils/")

    Logger.getLogger("org").setLevel(Level.OFF)

    val apiKey = "LKzLNnPLEturupgMKocfSvFrV"
    val apiSecret = "W6VsgeYEbSR4fjsjUqSNtmcwe0YN3A4Xe6yrgxRUHllNlGhAOc"

    val accessToken = "1386267320444669954-GRTRcruku5LsADXXHcNPYIEhfdyTUL"
    val accessTokenSecret = "NgZNYckTHN74beOlX2tqoEYPdlk4gcbffWW5CRV0Yffqr"

    System.setProperty("twitter4j.oauth.consumerKey", apiKey)
    System.setProperty("twitter4j.oauth.consumerSecret", apiSecret)
    System.setProperty("twitter4j.oauth.accessToken", accessToken)
    System.setProperty("twitter4j.oauth.accessTokenSecret", accessTokenSecret)

    val spark = SparkSession.builder()
      .master("local[2]")
      .appName("TwitterProducer")
      .getOrCreate()

    val sc = spark.sparkContext
    val ssc = new StreamingContext(sc, Seconds(5))
    val tweetsStream = TwitterUtils.createStream(ssc, None)

    val tweetStream = tweetsStream.map(t => (t.isRetweet, t.isFavorited, t.getUser.getStatusesCount,
      t.getUser.getFollowersCount, t.getUser.getFriendsCount, t.getUser.getFavouritesCount, t.getUser.isProtected, t.getUser.isVerified,
    t.getUser.getScreenName, t.getLang, t.getText))

    val props = new Properties()
    props.put("bootstrap.servers", "172.18.131.29:9092")
    props.put("bootstrap.server", "172.18.131.29:9092")
    props.put("acks", "all")
    props.put("retries", "0")
    props.put("batch.size", "16384")
    props.put("buffer.memory", "33554432")
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

    tweetStream.foreachRDD(rdds => {
      rdds.foreachPartition(partition => {
        val producer = new KafkaProducer[String, String](props)

        partition.foreach(tweet => {
          println(tweet)
          val record = new ProducerRecord("tweets-topic", "tweet-key",
            s"${tweet._1},${tweet._2},${tweet._3},${tweet._4},${tweet._5},${tweet._6},${tweet._7},${tweet._8},${tweet._9},${tweet._10},${tweet._11}")
          producer.send(record)
          println("Sent out tweet!")
        })
      })
    })

    ssc.start()
    ssc.awaitTermination()
  }
}