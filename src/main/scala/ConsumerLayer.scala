import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.log4j.{Level, Logger}
import org.apache.spark.streaming.kafka010.{ConsumerStrategies, KafkaUtils, LocationStrategies}
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.tree.DecisionTree
import org.elasticsearch.spark._

object ConsumerLayer {
  System.setProperty("hadoop.home.dir", "file:///C:/winutils/")

  def boolToInt(b: Boolean): Int = if (b) 1 else 0

  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.OFF)

    val conf = new SparkConf().setAppName("TwitterConsumer").setMaster("local[2]")
    val ssc = new StreamingContext(conf, Seconds(10))
    val sc = ssc.sparkContext

    val topicSet = Set("tweets-topic")
    val kafkaParm = Map[String, Object](
      ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG -> "172.18.131.29:9092",
      ConsumerConfig.GROUP_ID_CONFIG -> "tweet.group1",
      ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG -> classOf[StringDeserializer],
      ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG -> classOf[StringDeserializer],
    )

    val events = KafkaUtils.createDirectStream[String, String](ssc, LocationStrategies.PreferConsistent,
      ConsumerStrategies.Subscribe[String, String](topicSet, kafkaParm))

    val dataSource = sc.textFile("src/main/resources/project")
    val header = dataSource.first()
    val data = dataSource
      .filter(row => row != header)
      .map(l => l.split(","))
      .map(l => l.map(elm => elm.toInt))

    val parsedData = data.map(l => {
      val features = Vectors.dense(l(1), l(2), l(3), l(4), l(5), l(6), l(7), l(8))
      LabeledPoint(l(9), features)
    }).cache()

    val model = DecisionTree.trainClassifier(parsedData, 3, Map[Int, Int](), "gini", 5, 32)
    println("Starting to parse events")
    events.foreachRDD(rdd => {
      println("Got rdd")
      rdd.map(record => {
        val tweet = record.value().split(",")
        println(s"Got tweet from kafka ${record.value()}")
        //retweeted,favorited,statuses_count,followers_count,friends_count,favourites_count,protected,verified,screenName,lang,text
        val tweetVector = Vectors.dense(boolToInt(tweet(0).toBoolean),
          boolToInt(tweet(1).toBoolean),
          tweet(2).toDouble,
          tweet(3).toDouble,
          tweet(4).toDouble,
          tweet(5).toDouble,
          boolToInt(tweet(6).toBoolean),
          boolToInt(tweet(7).toBoolean),
        )

        val prediction = model.predict(tweetVector)

        val tweetElastic = Map(
          "screenName" -> tweet(8),
          "lang" -> tweet(9),
          "text" -> tweet(10),
          "isRetweet" -> tweet(0),
          "isFavorited" -> tweet(1),
          "statusesCount" -> tweet(2),
          "followersCount" -> tweet(3),
          "friendsCount" -> tweet(4),
          "favoriteCount" -> tweet(5),
          "isProtected" -> tweet(6),
          "isVerified" -> tweet(7),
          "prediction" -> prediction
        )
        tweetElastic
      }).saveToEs("elasticsearch-twitter")

    })

    ssc.start()
    ssc.awaitTermination()
  }

}
