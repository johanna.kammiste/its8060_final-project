name := "Scala-spark-Sbt2"

version := "0.1"

scalaVersion := "2.12.7"

libraryDependencies += "org.apache.spark" %% "spark-core" % "2.4.0"
libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.4.0"
libraryDependencies += "org.apache.spark" %% "spark-streaming" % "2.4.0"
libraryDependencies += "org.apache.spark" %% "spark-kvstore" % "2.4.0"

libraryDependencies += "org.apache.bahir" %% "spark-streaming-twitter" % "2.4.0"
libraryDependencies += "org.apache.spark" %% "spark-streaming-kafka-0-10" % "2.4.0"

libraryDependencies += "org.twitter4j" % "twitter4j-stream" % "2.2.4"

libraryDependencies += "org.apache.kafka" %% "kafka" % "2.4.0"

libraryDependencies += "org.apache.spark" %% "spark-mllib" % "2.4.0"

libraryDependencies += "org.elasticsearch" %% "elasticsearch-spark-20" % "7.13.1"
